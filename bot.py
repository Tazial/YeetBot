# Copyright (c) 2018 Taizo Simpson
# This file is part of the YeetBot project
#
# YeetBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import discord
from discord.ext import commands
import re
import importlib
import math
import operator

class YeetBot(commands.Bot):
	"""An interface facing Discord, and the primary UI of the project.

	This class acts as a Discord bot, extending the discord.ext.commands.Bot class
	to do so.  This allows the end users of the bot to interact with and view the
	various things they have control over, with commands for each one.  It also
	includes an admin command group for administering the system through Discord.

	Attributes:
		bank (bank.Bank): The bank in which users are to be queried from and stored
			to.
		shop (shop.Shop): The shop object used to keep track of stock and
			determine the prices that users are charged.
		register (str): The link users should be sent to after entering the
			`register` command.
		channels (List[str]): A list of the IDs of Discord channels that the bot is
			allowed to interact on.  This does not include DMs, which the bot is
			always allowed on.
		module_config (Dict[dict or list]): The configuration of any modules
			that have their configuration set in the config file.  Defaults to
			an empty dict.
		admins (List[str]): A list containing the various Discord ids of users
			who are allowed to use administrative commands with the bot, namely
			those in the `debug` command group.
		config (dict): The full bot config, passed in through the `config`
			argument.
		pending_transactions (Set[str]): A list of Discord users who have
			currently pending transactions, i.e. have started a transaction, but
			the bot is still awaiting the response from the module responsable for
			completing the purchase.
	
	Args:
		bank (bank.Bank): See the attribute, `bank`.  During construction, the
		bot installs its own get_pay_modifier function in the bank.
		shop (shop.Shop): See the attribute, `shop`
		register_link (str): See the attribute, `register`
		module_config (dict): See the `module_config` attribute
		config (dict): A dict corresponding to the `bot` config section,
			containing the keys `channels` (corresponding to the `channels`
			attribute), `ranks` (see documentation for `get_pay_modifier`), and
			`admins` (corresponding to the `admins` attribute)
	"""
	
	def __init__(self, bank, shop, register_link, module_config, config):
		super().__init__(command_prefix='')
		self.bank = bank
		self.shop = shop
		self.register = register_link
		self.module_config = module_config
		self.channels = config['channels']
		self.admins = config['admins']
		self.config = config
		self.pending_transactions = set()
		self.bank.get_pay_modifier = self.get_pay_modifier
		self.add_command(commands.Command(
			'license',
			self.command_license,
			help='Print information about the code license and source code'
		))
		self.add_command(commands.Command(
			'balance',
			self.command_balance,
			aliases=['bal'],
			help='Print the your current balance',
			pass_context=True
		))
		self.add_command(commands.Command(
			'prices',
			self.command_prices,
			aliases = ['stock', 'offers'],
			help = 'Display current prices and items available for sale'
		))
		self.add_command(commands.Command(
			'buy',
			self.command_buy,
			aliases = ['purchase'],
			help = 'Purchase an item from the shop',
			pass_context=True
		))
		self.add_command(commands.Command(
			'register',
			self.command_register,
			help = 'Register your Discord account with the bot'
		))
		self.add_command(commands.Command(
			'pay',
			self.command_pay,
			aliases = ['send'],
			help='Send credits from your account to another user',
			pass_context=True
		))
		debug_group = commands.Group(
			name='debug',
			callback=self.command_debug,
			help='Administrative commands for developers and admins only'
		)
		debug_group.add_command(commands.Command(
			'add',
			self.command_debug_add,
			help='Add or subtract an amount of money to a user\'s balance.'
		))
		debug_group.add_command(commands.Command(
			'bal',
			self.command_debug_bal,
			help='Check a specific user\'s balance'
		))
		debug_group.checks.append(self.check_admin)
		self.add_command(debug_group)
		self.add_check(self.check_channels)
		self.description = 'In order to start earning and spending credits, you need to register with the bot.  Then, type `prices` to see what\'s for sale, and `buy <number>` when you see something you want'
	
	async def command_license(self):
		""" An informitive command that should simply provide the user with
		licensing information and a link to the source code, in compliance with the
		AGPL-3.0-or-greater.
		"""
		await self.say(self.config['license_message'])
	
	async def command_balance(self, context):
		""" A command to allow the user to check their own balance.

		Args:
			context (discord.ext.commands.Context): The context, as discord.py
				promises to provide.  The only important attribute is the message
				author's id
		"""
		bal = self.bank.get_balance(context.message.author.id)
		embed=discord.Embed()
		embed.add_field(name=f"{context.message.author.name}'s balance is", value = str(bal))
		await self.say(embed=embed)
		if bal == None:
			await self.say('Type `register` to start earning money')
	
	async def command_prices(self):
		"""Responds with current offers, including the name, price, & stock.
		"""
		items = list(self.shop.items)
		for page in range(math.ceil(len(items)/25)):
			embed = discord.Embed(title=f'Available Offers [Page {page + 1}]');
			for i in range(page * 25, min(len(items),(page + 1) * 25)):
				item = items[i]
				embed.add_field(name=f'{i+1}. {item.name}', value = f'{item.price} credits, {item.stock} in stock')
			await self.say(embed=embed)
	
	async def command_buy(self, context, id : int):
		"""Purchases a certain number of the given item

		Args:
			context (discord.ext.commands.Context): The context, as discord.py
				promises to provide.  The only important attribute is the message
				author's id
			id (int): One more than the index of the item that is to be purchased.
				Should correspond to the number given by the `prices` command.
		"""
		item = list(self.shop.items)[id - 1]
		
		user = context.message.author.id
		bal = self.bank.get_balance(user)
		cost = item.price
		plural = 's' if cost > 1 else ''
		if bal == None:
			await self.say(embed=discord.Embed(description='Unfortunately, I don\'t see you in our database.  In order to receive credits for time spent in game, please type `register`, and I will give you a link to sign in with Discord.'))
		elif not item.stock:
			await self.say(embed=discord.Embed(title=f'The shop is currently out of stock of {item.name}.  Check back in a while to see if stock has replenished'))
		elif bal < item.price:
			await self.say(embed=discord.Embed(title=f'It doesn\'t look like you have enough credits to purchase this item.  Your current balance is {bal} credit{plural}, and this {item.name} costs {cost} credit{plural}'))
		elif user in self.pending_transactions:
			await self.say(embed=discord.Embed(title='Error', description=f'You already have pending transactions!  Please complete these before purchasing anything else.'))
		else:
			self.pending_transactions.add(user)
			module = importlib.import_module('modules.%s'%item.config['module'])
			success = await module.on_purchase(context.message.author, item, self, self.module_config[item.config['module']])
			if success:
				self.bank.change_balance(user, -item.price)
				item.stock -= 1
				await self.say(embed=discord.Embed(title=f'{context.message.author} successfully purchased {item.name} for {cost} credit{plural}'))
			else:
				await self.say(embed=discord.Embed(title='Error', description=f'{context.message.author}\'s purchase of {item.name} did not complete successfully.  The transaction was either canceled or failed.'))
			self.pending_transactions.remove(user)

	async def command_register(self):
		"""Sends the user a link they can use to register with the bot
		(actually, the `Bank`.  This is the same link that was passed into the
		bot when it was constructed.
		"""
		embed = discord.Embed(title = "Please click the link to register");
		embed.url = self.register
		await self.say(embed=embed)
	
	async def command_pay(self, context, recipient: discord.User, amt : int):
		user = context.message.author.id
		user_bal = self.bank.get_balance(user)
		plural = 's' if amt > 1 else ''
		if user_bal is None:
			await self.say(embed=discord.Embed(title='Error', description='Unfortunately, I don\'t see you in our database.  In order to receive credits for time spent in game, please type `register`, and I will give you a link to sign in with Discord.'))
		elif user_bal < amt:
			await self.say(embed=discord.Embed(title='Error', description=f'{context.message.author}, you cannot pay {recipient} {amt} credit{plural} because your don\'t have enough credits yourself.'))
		else:
			success = self.bank.change_balance(recipient.id, amt)
			if success:
				self.bank.change_balance(user, -amt)
				await self.say(embed=discord.Embed(title='Payment Success!',description=f'{context.message.author} successfully paid {amt} credits to {recipient}'))
			else:
				await self.say(embed=discord.Embed(title='Error', description=f'Failed to pay {recipient}.  Have they registered with the `register` command?'))
	
	async def command_debug_add(self, user : discord.User, amt : int):
		"""An administrative command to manually add or subtract money to a user's
		account.

		Args:
			user (discord.User): The user whose balance is to be affected
			amt (int): The amount to change to user's balance by
		"""

		if self.bank.change_balance(user.id, amt):
			await self.say(f'Added {amt} to {user.name}\'s account')
		else:
			await self.say('User not found.  Are they registered?')
	
	async def command_debug_bal(self, user : discord.User):
		"""An administrative command for viewing the balance of a specific user

		Args:
			user (discord.User): The user whose balance is to be checked
		"""
		bal = self.bank.get_balance(user.id)
		if bal != None:
			await self.say(f'The user {user.name} has {bal} credits')
		else:
			await self.say(f'The user {user.name} is not registered with the system')
		
	async def command_debug(self):
		pass
	
	def check_channels(self, ctx):
		"""A check method to ensure that the bot only responds on channels it has
		been created to talk on.

		Args:
			ctx (discord.ext.commands.Context): The context of the incomming message

		Returns:
			bool: True if the bot is allowed to speak in this context, False
				otherwise
		"""
		return ctx.message.channel.id in self.channels
	
	def check_admin(self, ctx):
		"""A check method ensuring that the user running the command is allowed
		to administer the bot, i.e. is in the `admins` attribute.

		Args:
			ctx (discord.ext.commands.Context): The context of the incomming message

		Returns:
			bool: True if the user is an allowed admin, False otherwise.
		"""
		return ctx.message.author.id in self.admins
	
	async def on_ready(self):
		"""A command corresponding to discord.py's on_ready event, printing a basic
		alert to the console about the discord bot being logged in.
		"""
		print(f'[INFO] Logged in as {self.user.name}')
	
	def get_pay_modifier(self, discord_id):
		"""Determine what modifier should be applied to a user's income

		This number is applied to any income a user makes.  This is based off
		of the Discord rank of the user in a specific channel, as determined by
		information passed in through the config argument (supposedly tied to a
		config file).

		Specifically, the "server" key of the config should contain the Discord ID
		of the server on which the roles are watched, and the "roles" key
		should contain a dict, with the keys being the name of each role (just
		for readability, not used), and the values being YET ANOTHER dict, with
		three keys, "id", "paymod", and "priority", corresponding to the
		Discord id of the role, the rate that that role recieves money, and the
		priority that role takes over other roles.  If a user is found with two
		matching roles, the rate of the one with higher priority is used.
		
		The defaults to returning one if no applicable roles are found.
		
		Example:
			# This is an example structure of the config being passed in,
			# structured in YAML
			ranks:
				server: 345393743421243403 # Looking at roles in this server
				roles:
					low_ranking_role:
						id: 448657946981957632
						paymod: 1.5
						priority: 1
					high_ranking_role:
						id: 459161418546020353
						payrate: 100
						priority: 2
		Args:
			discord_id (str): The Discord ID of the user to find the modifier for

		Returns:
			Number: A floating point number to be multiplied against the income
			of the user
		"""
		# I think this entire thing could be one line, and I was almost tempted
		# to try it, but I took pity on you.
		paid_roles = self.config['ranks']['roles']
		member = self.get_server(str(self.config['ranks']['server'])).get_member(discord_id)
		if member:
			user_roles = [role.id for role in member.roles]
			applicable_roles = [role for role in paid_roles.values() if str(role['id']) in user_roles]
			return max(applicable_roles, key=operator.itemgetter('priority'), default={'paymod':1})['paymod']
		else:
			return 1
