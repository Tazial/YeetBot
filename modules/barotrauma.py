# Copyright (c) 2018 Taizo Simpson
# This file is part of the YeetBot project
#
# YeetBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import discord
import os
import re
from datetime import date, datetime, timedelta
import operator
import subprocess

async def on_purchase(user, item, bot, config):
	""" Event triggered when an item with this module is purchased

	This is used to set actions to correspond to this purchase.  Anything
	scriptable with python should be accomplishable.  For example, an item could be
	configured to correspond to an in-game item by adding a config variable for
	that item with it's in-game id, and the configuring on_purchase to spawn an
	item in-game for the user with the specified id

	Args:
		user (discord.User): The Discord user who purchased the item
		item (shop.Item): The item that was purchased.  Please treat this as
			read-only unless you really know what you're doing.
		bot (discord.ext.commands.bot.Bot): The Discord bot handling this purchase
		config (dict): A dict containing one value, under the key 'command', with
			the command to be used to send commands to be barotrauma server, with
			%s substituting the command to be sent
	Returns:
		bool: True if the command went through successfully, False otherwise.
		In this case, False is returned only if the user canceled the action.
	"""
	await bot.send_message(user, f'You just purchased {item.name}.  Please enter the name of the player you would like to receive this item.  Make sure that the name **exactly** matches the name of an in-game player, and that the round has started.  If you have changed your mind, please type `cancel`.')
	while True: # Until a valid message is recieved as a PM
		resp = (await bot.wait_for_message(author=user))
		if resp.channel.type == discord.ChannelType.private: # The channel is a 1:1 PM
			if resp.content.lower() == 'cancel':
				await bot.send_message(user, f'Purchase canceled')
				return False
			else:
				await bot.send_message(user, f'Sending {item.name} to {resp.content}...')
				command = config['command']%(f'spawnitem {item.config.get("id", item.name)} "{clean_name(resp.content)}"')
				subprocess.call(command, shell=True)
				command = config['command']%(f'msg [Shop] {clean_name(resp.content)} just bought {item.name} from the Discord shop')
				subprocess.call(command, shell=True)
				return True

log_cache = {} # Dict of file name to (last-mod-date, profile builder) tupple
def get_total_pay(user, config):
	"""Finds the total amount this user has earned as a function of logged playtime.

	Specifically, the amount any player gets is 10 units for every hour played,
	rounded down.  This is calculated by analyzing the logs in a directory
	passed through the `log_dir` key in the config argument.  The money is
	awarded based on ip address, not by player name, as that is the only
	concrete proof that a user is who they say they are.

	Args:
		user (bank.User): The user to find the pay of
		config (dict): The module config section
	
	Returns:
		Number: The total amount owed to the user.  This value can be a
		fraction, but it will be rounded out by the time it reaches the user.
	"""
	# Setup
	logs = (os.path.join(tup[0], name) for tup in os.walk(os.path.expanduser(config['log_dir'])) for name in tup[2])

	# Reading new logs
	for log in logs:
		if log not in log_cache or log_cache[log][0] < os.path.getmtime(log):
			new_builder = ProfileBuilder()
			new_builder.add_log_path(log)
			log_cache[log] = (os.path.getmtime(log), new_builder)
	
	# Check for user profile
	return sum(tup[1].get_profile(user.ip_address).total_time().total_seconds() /60 /60 *10 for tup in log_cache.values() if tup[1].get_profile(user.ip_address))

def clean_name(name):
	"""Escape a client's name

	Specifically, this first escapes any double quotes and backslashes so that
	barotrauma can properly parse the name as long as it is surrounded in
	double quotes.  Then, the names has any single quotes escaped with a single
	back slash, and any backslashes from the previous sanitization are escaped
	as well.  This makes it so that when wrapped in single quotes and passed
	through bash into barotrauma.

	Args:
		name (str): An unsanitized player name

	Returns:
		str: A sanitized version of the `name` argument

	"""
	return name.replace('\\','\\\\\\\\').replace('"','\\\\"').replace("'","\\'")

# The following  was written as a docstring for the ProfileBuilder and
# associated classes while they were all stored in a seperate module.  Now they
# have been merged, but this still contains a useful explanation for how this
# section works.

# Collect user's playtime information from Barotrauma logs
# 
# This class contains a number of utilities for collecting information
# information about the playsessions of barotrauma players.  This information
# is collected using a `ProfileBuilder`, and stored per user in `Profile`s.
# Each profile consists of several `Connection`s, which act as one single
# session, which in turn contains a single `Disconnect`, marking the point the
# player disconnected, ending that play session.

class ProfileBuilder:
	"""A collection of `Profile` classes and methods to add to them

	This class is used to add information to multiple profiles at once, by
	feeding in a series of log entries and applying them to the relevant
	profile.  This simplifies the proccess of creating profiles, as whole logs
	can now be dumped into this class, and the resulting profiles simply pulled
	out.

	Attributes:
		profiles (List[Profile]): A list containing all of the profiles
			generated from this builder so far.
	"""
	
	def __init__(self):
		self.profiles = []

	def add_log_path(self,log_path):
		"""A handy shorthand for adding a log through a file path instead of lines

		Args:
			log_path (str): The relative or absolute path to a barotrauma log file.
		"""
		with open(log_path) as log:
			self.add_log(log.readlines())
	
	def add_log(self,log_lines):
		"""Analyze a log file and update the profiles

		This is the central method of the `ProfileBuilder`.  Logs are passed in
		through this method, and then parsed and analyzed in order to construct
		a picture of each user's sessions, which are then added to the
		appropriate user's `Profile`.

		Note that this methods is designed to take only one log file at a time,
		and should recieve the whole log, not just an exerpt.  This is because
		the `ProfileBuilder` assumes that the log starts with no players logged
		in, and that all players disconnect at the time of the last message in
		the log.

		Args:
			log_lines (List[str]): A list containing all lines in the log, each
				line as a single string
		"""
		# Group 1: Time
		# Group 2: Name
		# Group 3: IP
		connections = [
			Connection.from_regex(match)
			for match in
			[
				re.match(r'^\[([^\[\]]+)\] (?:New Player )?(.+) \(((\d+\.){3}\d+)\) has joined(?: - Previously \(.+\))?\.$',line)
				for line in
				log_lines
			]
			if match
		]
		disconnects = [
			Disconnect.from_regex(match)
			for match in
			[
				re.match(r'^\[([^\[\]]+)\] (.+) has (disconnected|been kicked from the server.|left the server)$', line)
				for line in
				log_lines
			]
			if match
		]

		
		# Pair connections and disconnection
		last_message = re.match('^\[([^\[\]]+)\]', log_lines[-1])
		assert last_message, 'Unrecognized log format'
		last_message_time = datetime.strptime(last_message.group(1), '%m/%d/%Y %I:%M:%S %p')
		for conn in connections:
			for disc in disconnects:
				if conn.matches(disc):
					conn.disconnect = disc
					break
			else:
				conn.disconnect = Disconnect(conn.name, last_message_time)
			self.add_connection(conn)
	
	def get_profile(self, ip):
		""" Finds the profile for the specified ip address and returns it

		Args:
			ip (str): The ip of the profile being found

		Returns:
			Profile: The profile for the provided ip, or None if this ip has
			never been used in the logs analyzed so far.
		"""
		return next((prof for prof in self.profiles if prof.ip == ip), None)

	def add_connection(self, connection):
		""" Add the provided connection to the appropriate profile

		This is really only for internal use, but I guess you can use it too.
		This finds the profile that shares an IP with the provided connection,
		and adds the connection to the profile's list of play sessions

		Args:
			connection (Connection): The `Connection` to be added to the
				profile builder
		"""
		prof = self.get_profile(connection.ip)
		if not prof:
			prof = Profile(connection.ip)
			self.profiles.append(prof)
		prof.add_connection(connection)
	
class Disconnect:
	"""Representative of the moment a user disconnects from the server

	Attributes:
		name (str): The name that the player who was connected used
		date (datetime.datetime): The moment the player disconnected
		kick (bool): True if the user was kicked forcefully.  Defaults to False
	Args:
		name (str): See `name` attribute
		date (datetime.datetime): See `date` attribute
		kick (bool): See `kick` attribute
	"""

	def __init__(self, name, date, kick=False):
		self.name = name
		self.date = date
		self.kick = kick

	def from_regex(regex):
		"""An alternate constructor for building a Disconnect out of a regex match

		The specific format of the regex is a name in group 2, a barotrauma
		timestamp in group 1, and a phrase optionally containing the word kick
		in group three.  This format matches the typical barotrauma log format,
		and specifically, the regex:

		    r'^\[([^\[\]]+)\] (.+) has (disconnected|been kicked from the server.|left the server)$', line
		"""
		name = regex.group(2)
		conn_date = datetime.strptime(regex.group(1), '%m/%d/%Y %I:%M:%S %p')
		return Disconnect(name, conn_date, kick='kicked' in regex.group(3))
	
	def __str__(self):
		return '%s disconnected on %s at %s'%(self.name, self.ip, date.strftime('%Y-%m-%d %H:%M:%S',self.date))

class Connection:
	"""Represents the moment a user connects, can be paired with Disconnect

	This represents the moment a user connects, until it is paired with a
	`Disconnect`, at which point it represents the full user's session.

	Attributes:
		name (str): The payer's display name
		date (datetime.datetime): The date and time that the player connected
		ip (str): The player's ip address
		disconnect (Disconnect): An object representing when and how the user
			disconnected and ended this play session.
	
	Args:
		name (str): See the `name` attribute
		date (datetime.datetime): See the `date` attribute
		ip (str): See the `ip` attribute
	"""
	
	def __init__(self, name, date, ip):
		self.name = name
		self.date = date
		self.ip = ip
		self.disconnect = None
	
	def from_regex(regex):
		"""An alternate constructor for building a Connection out of a regex match

		The specific format of the regex is a name in group 2, a barotrauma
		timestamp in group 1, and the user's ip address in group 3.  This
		format matches the typical barotrauma log format, and specifically, the
		regex:

		r'^\[([^\[\]]+)\] (?:New Player )?(.+) \(((\d+\.){3}\d+)\) has joined\.$'
		"""
		name = regex.group(2)
		ip = regex.group(3)
		conn_date = datetime.strptime(regex.group(1), '%m/%d/%Y %I:%M:%S %p')
		return Connection(name, conn_date, ip)
	
	def matches(self,disconnect):
		"""Checks to see if this connection is compatable with a `Disconnect`

		Specifically, it checks that the name on both the `Disconnect` and the
		`Connection` match, and that the disconnect happened after the
		connection.  If this is the case, the two are compatable.

		Args:
			disconnect (Disconnect): The `Disconnect` to be tested

		Returns:
			bool: True if the disconnect is compatable, False otherwise
		"""
		return\
			self.name == disconnect.name and\
			disconnect.date - self.date > timedelta(0)
	
	def duration(self):
		"""Calculates the length of this specific play session.

		Returns:
			datetime.timedelta: The time between the user's connection was open
				and when it was closed.  If a `disconnect` has not been set, this
				returns 0 seconds.
		"""
		if self.disconnect:
			return self.disconnect.date - self.date
		else:
			return timedelta(0)
	
	def __str__(self):
		return '%s connected on %s'%(self.name, self.ip) + (
			' at %s'%(self.date.strftime('%Y-%m-%d %H:%M:%S'))
			if not self.disconnect else 
			' from %s to %s (%s)'%(self.date.strftime('%Y-%m-%d %H:%M:%S'), self.disconnect.date.strftime('%Y-%m-%d %H:%M:%S'), self.disconnect.date - self.date)
		) + ('*' if self.disconnect and self.disconnect.kick else '')
	
	def __eq__(self, other):
		return self.date == other.date

class Profile:
	"""A collection of play sessions for a specific ip.

	Specifically, play sessions are represented through `Connection` objects,
	and this class provides a few methods for analyzing these sessions, like
	looking at all of the names this player has used and the total time they've
	spent on the server.  This does not account for the fact that a player
	might use two or more ips, nor does it recognize the fact that more than
	one player might use one IP.

	Attributes:
		ip (str): The ip address to collect connections for
		connections (list[Connection]): A list of all of the connections of
			this player contained in this profile
	
	Args:
		ip (str): See the `ip` attribute
	"""

	def __init__(self, ip):
		self.connections = []
		self.ip = ip
	
	def add_connection(self,connection):
		"""Add a connection to existing list of connections.

		Args:
			connection (Connection): The connection to add
		"""
		assert connection.ip == self.ip, "The provided connection does not match the ip address of the profile"
		self.connections.append(connection)
	
	def get_names(self):
		"""A set containing all of the display names this user has used.

		Returns:
			set[Connection]: See above
		"""
		return set([conn.name for conn in self.connections])
	
	def total_time(self):
		"""The total time spent over all play sessions by this user

		Returns:
			datetime.timedelta: See above
		"""
		return sum((conn.duration() for conn in self.connections),timedelta(0))
	
	def __str__(self):
		output = "Profile for %s:\n"%self.ip
		output += "\tPlayed as: %s\n"%', '.join(self.get_names())
		output += "\tTotal Playtime: %s\n"%self.total_time()
		output += "\t%d Total Connections:\n"%len(self.connections)
		output += '\n'.join(['\t\t%s'%(
				conn.date.strftime('%Y-%m-%d %H:%M:%S')
				if not conn.disconnect else
				'%s to %s (%s)'%(conn.date.strftime('%Y-%m-%d %H:%M:%S'), conn.disconnect.date.strftime('%Y-%m-%d %H:%M:%S'), conn.disconnect.date - conn.date)
			) + ('*' if conn.disconnect and conn.disconnect.kick else '')
			for conn in self.connections])
		return output
